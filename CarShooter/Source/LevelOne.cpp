#include "../Headers/Const.h"
#include "../Headers/LevelOne.h"

#include <iostream>

namespace CoreGame {

	using namespace sf;

	LevelOne::LevelOne(RenderWindow& aWindow) 
		: mWindow(aWindow)
		, mTextures(TextureHandler())
		, mPlayerVehicle(new ClassVehicle::PlayerVehicle(30.0f, 62.5f,
			mTextures.getTexture(YELLOW_CAR), 35.0f, 128.0f, 983.0f))
		, mCheckpoints(new Vector2f[24])
		, mView(View(Vector2f(0.0f, 0.0f),
					 Vector2f(800.0f, 600.0f)))
	{

		mBackground.setTexture(*mTextures.getTexture(BG_LEVEL_ONE));
		mBackground.setScale(2, 2);

		mWindow.setView(mView);

		Vector2f checkpointsArray[] = {
				Vector2f(155, 690),		Vector2f(225,225),
				Vector2f(625,225),		Vector2f(855,605),
				Vector2f(1230,605),		Vector2f(1385, 205),
				Vector2f(1800, 205),	Vector2f(1905, 525),
				Vector2f(1810, 815),	Vector2f(1550, 890),
				Vector2f(1255, 975),	Vector2f(960, 1050),
				Vector2f(690, 1185),	Vector2f(1065, 1265),
				Vector2f(1460, 1265),	Vector2f(1850, 1345),
				Vector2f(1850, 1765),	Vector2f(1300, 1900),
				Vector2f(1210, 1650),	Vector2f(930, 1650),
				Vector2f(625, 1935),	Vector2f(300, 1935),
				Vector2f(155, 1515),	Vector2f(155, 1130) 
		};

		for (int i = 0; i < 24; ++i) {
			mCheckpoints[i] = checkpointsArray[i];
		}
		
		// Setting Enemies
		setEnemies();

		// Setting the Map Obstacles
		setObstacles();

	}

	LevelOne::~LevelOne() {
		delete mPlayerVehicle;
		delete[] mCheckpoints;

		for (std::vector<ClassVehicle::EnemyVehicle *>::iterator
			it = mEnemiesList.begin(); it != mEnemiesList.end();) {
			delete *it;
			it = mEnemiesList.erase(it);
		}

	}

	// Makes the player shoot
	void LevelOne::shootBullet(float& deltaTime) {
		mPlayerVehicle->shootBullet(deltaTime);
	}

	// Checks the colitions of the bullets
	void LevelOne::handleBullets(ClassVehicle::Vehicle& aCar) {

		std::vector<Weapon::Bullet>& bullets = aCar.getBullets();

		for (std::vector<ClassVehicle::EnemyVehicle *>::iterator enemiesIt = mEnemiesList.begin();
			enemiesIt != mEnemiesList.end(); ++enemiesIt) {
			for (std::vector<Weapon::Bullet>::iterator bulletIT = bullets.begin();
				bulletIT != bullets.end(); ) {

				if (bulletIT->getCollider().ckeckCollision((*enemiesIt)->getCollider(), 0.1f)) {
					bulletIT = bullets.erase(bulletIT);
					(*enemiesIt)->setBulletDemage();
				} else {
					bulletIT++;
				}
			}
		}

	}

	void LevelOne::generalMovement(float& deltaTime) {

		Vector2f playerPosition = mPlayerVehicle->getPosition();

		// Movement of the player
		mPlayerVehicle->animationMovement(deltaTime);

		obstacleColide(*mPlayerVehicle);
		mView.setCenter(playerPosition);
		handleBullets(*mPlayerVehicle);
		mWindow.setView(mView);

		// Movement and aliveness of Enemies
		for (std::vector<ClassVehicle::EnemyVehicle *>::iterator enemiesIt = mEnemiesList.begin();
			enemiesIt != mEnemiesList.end();) {
			(*enemiesIt)->inteligentMovement(playerPosition);
			obstacleColide(**enemiesIt);
			handleBullets(**enemiesIt);

			// Checking if someone died
			if ((*enemiesIt)->isDead()) {

				// Setting the explotion where the enemy dies
				mExplotions.push_back( new ClassVehicle::Explotion(mTextures.getTexture(EXPLOTION),
					Vector2u(5, 5), 0.03f, (*enemiesIt)->getPosition()));

				// Deleting the enemy
				delete (*enemiesIt);
				enemiesIt = mEnemiesList.erase(enemiesIt);

			} else {
				enemiesIt++;
			}

		}

		// Setting the background
		mWindow.draw(mBackground);

		// Setting bullets if there's anyone
		mPlayerVehicle->drawBullet(mWindow);

		// Setting the Enemies
		for (std::vector<ClassVehicle::EnemyVehicle *>::iterator it = mEnemiesList.begin();
			it != mEnemiesList.end(); ++it) {
			mWindow.draw((*it)->getModel());
		}

		// Setting the player 
		mWindow.draw(mPlayerVehicle->getModel());

		for (std::vector<ClassVehicle::Explotion *>::iterator explotionsIT = mExplotions.begin();
			explotionsIT != mExplotions.end(); ) {
			(*explotionsIT)->update(deltaTime);
			(*explotionsIT)->draw(mWindow);


			if ((*explotionsIT)->isFinished()) {
				delete (*explotionsIT);
				explotionsIT = mExplotions.erase(explotionsIT);
			} else {
				explotionsIT++;
			}

		}
	}

	void LevelOne::obstacleColide(ClassVehicle::Vehicle& aCar) {

		for (std::vector<Collider::Obstacle>::iterator it = mObstaclesList.begin();
			it != mObstaclesList.end(); ++it) {

			// Checks is "A Car" colides with a obstacle
			if (it->getCollider().ckeckCollision(aCar.getCollider(), 1.0f)) {
				aCar.setSpeed(aCar.getSpeed() / 2);
			}
		}

		for (std::vector<ClassVehicle::EnemyVehicle *>::iterator it = mEnemiesList.begin();
			it != mEnemiesList.end(); ++it) {

			// Checks if the "A Car" colides with another enemy
			(*it)->getCollider().ckeckCollision(aCar.getCollider(), 0.5f);
		}

	}

	  /*-----------------------------------------*/
	 //// Private Method. Setting the Enemies ////
	/*-----------------------------------------*/
	void LevelOne::setEnemies() {

		Texture* redCar = mTextures.getTexture(RED_CAR);

		mEnemiesList.push_back(new ClassVehicle::EnemyVehicle(30.0f, 62.5f,
			redCar, 35.0f, 65.0f, 85.0f, mCheckpoints, 24));
		mEnemiesList[0]->rotateVehicle(90);

		mEnemiesList.push_back(new ClassVehicle::EnemyVehicle(30.0f, 62.5f,
			redCar, 35.0f, 1555.0f, 310.0f, mCheckpoints, 24));

		mEnemiesList.push_back(new ClassVehicle::EnemyVehicle(30.0f, 62.5f,
			redCar, 35.0f, 580.0f, 950.0f, mCheckpoints, 24));
		mEnemiesList[2]->rotateVehicle(180);

		mEnemiesList.push_back(new ClassVehicle::EnemyVehicle(30.0f, 62.5f,
			redCar, 35.0f, 1955.0f, 1250.0f, mCheckpoints, 24));
		mEnemiesList[3]->rotateVehicle(180);

		mEnemiesList.push_back(new ClassVehicle::EnemyVehicle(30.0f, 62.5f,
			redCar, 35.0f, 800.0f, 1990.0f, mCheckpoints, 24));
		mEnemiesList[4]->rotateVehicle(270);

		mEnemiesList.push_back(new ClassVehicle::EnemyVehicle(30.0f, 62.5f,
			redCar, 35.0f, 1230.0f, 720.0f, mCheckpoints, 24));
		mEnemiesList[5]->rotateVehicle(270);

	}

	  /*-------------------------------------------*/
	 //// Private Method. Setting the Obstacles ////
	/*-------------------------------------------*/
	void LevelOne::setObstacles() {
		mObstaclesList.push_back(Collider::Obstacle(Vector2f(206.0f, 1390.0f),
			Vector2f(336.0f + 103.0f, 322.0f + 695.0f)));

		mObstaclesList.push_back(Collider::Obstacle(Vector2f(414.0f, 110.0f),
			Vector2f(544 + 207, 754 + 55)));

		mObstaclesList.push_back(Collider::Obstacle(Vector2f(222.0f, 464.0f),
			Vector2f(912 + 111, 232.0f + 0.0f)));

		mObstaclesList.push_back(Collider::Obstacle(Vector2f(222.0f, 350.0f),
			Vector2f(1472.0f + 111.0f, 370.0f + 175.0f)));

		mObstaclesList.push_back(Collider::Obstacle(Vector2f(318.0f, 46.0f),
			Vector2f(960.0f + 159.0f, 1122.0f + 23.0f)));

		mObstaclesList.push_back(Collider::Obstacle(Vector2f(350.0f, 126.0f),
			Vector2f(1280.0f + 175.0f, 1042.0f + 63.0f)));

		mObstaclesList.push_back(Collider::Obstacle(Vector2f(398.0f, 190.0f),
			Vector2f(1632.0f + 199.0f, 978.0f + 95.0f)));

		mObstaclesList.push_back(Collider::Obstacle(Vector2f(110.0f, 238.0f),
			Vector2f(544.0f + 55.0f, 1474.0f + 119.0f)));

		mObstaclesList.push_back(Collider::Obstacle(Vector2f(750.0f, 62.0f),
			Vector2f(656.0f + 383.0f, 1474.0f + 31.0f)));

		mObstaclesList.push_back(Collider::Obstacle(Vector2f(286.0f, 174.0f),
			Vector2f(1408.0f + 143, 1474.0f + 87.0f)));

		mObstaclesList.push_back(Collider::Obstacle(Vector2f(190.0f, 158.0f),
			Vector2f(950.0f + 95.0f, 1760.0f + 79.0f)));

		mObstaclesList.push_back(Collider::Obstacle(Vector2f(46.0f, 126.0f),
			Vector2f(448.0f + 23.0f, 1714.0f + 63.0f)));

		// Origin Y
		mObstaclesList.push_back(Collider::Obstacle(Vector2f(0.0f, 2048.0f),
			Vector2f(16.0f + 0.0f, 0.0f + 1024.0f)));

		// Origin X
		mObstaclesList.push_back(Collider::Obstacle(Vector2f(2048.0f, 0.0f),
			Vector2f(0.0f + 1024.0f, 0.0f + 16.0f)));

		// Limit Y
		mObstaclesList.push_back(Collider::Obstacle(Vector2f(0.0f, 2048.0f),
			Vector2f(2032.0f + 0.0f, 0.0f + 1024.0f)));

		// Limit X
		mObstaclesList.push_back(Collider::Obstacle(Vector2f(2048.0f, 0.0f),
			Vector2f(0.0f + 1024.0f, 2032.0f + 0.0f)));
	}

}