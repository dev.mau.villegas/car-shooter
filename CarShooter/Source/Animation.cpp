#include "../Headers/Animation.h"
#include <SFML/Graphics.hpp>

namespace CoreGame {

	Animation::Animation(sf::Texture* aTexture,
		sf::Vector2u aImageCount, float aSwitchtime)
		: mImageCount(aImageCount)
		, mCurrentImage(0, 0)
		, mTotalTime(0.0f)
		, mSwitchTime(aSwitchtime)
	{
		uvRect.width = aTexture->getSize().x / float(aImageCount.x);
		uvRect.height = aTexture->getSize().y / float(aImageCount.y);

	}

	Animation::Animation(const Animation& aAnimation)
		: mImageCount	(aAnimation.mImageCount)
		, mCurrentImage	(aAnimation.mCurrentImage)
		, mTotalTime	(aAnimation.mTotalTime)
		, mSwitchTime	(aAnimation.mSwitchTime)
	{
		uvRect.width =	aAnimation.uvRect.width;
		uvRect.height = aAnimation.uvRect.height;
	}

	Animation::~Animation() {

	}

	void Animation::update(unsigned int aRow, float deltaTime) {

		mCurrentImage.y = aRow;
		mTotalTime += deltaTime;
		if (mTotalTime >= mSwitchTime) {
			mTotalTime -= mSwitchTime;
			mCurrentImage.x++;

			if (mCurrentImage.x >= mImageCount.x) {
				mCurrentImage.x = 0;
			}
		}

		uvRect.left = mCurrentImage.x * uvRect.width;
		uvRect.top = mCurrentImage.y * uvRect.height;

	}

}