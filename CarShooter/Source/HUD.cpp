#define _CRT_SECURE_NO_WARNINGS
#include "../Headers/HUD.h"

namespace CoreGame {

	HUD::HUD(unsigned int aEnemiesCount)
		: mEnemiesCount(aEnemiesCount)
	
	{

		if (!mHUDFont.loadFromFile("Textures/arial.ttf")) {
			// Handle Error
		}

		mHUD.setFont(mHUDFont);
		mHUD.setFillColor(sf::Color::White);

		mHUD.setString(_itoa(mEnemiesCount, mEnemiesLeft, 10));
		mHUD.setCharacterSize(50);
	}

	HUD::HUD(const HUD & aHUD)
		: mHUDFont(aHUD.mHUDFont)
		, mHUD(aHUD.mHUD)
		, mEnemiesCount(aHUD.mEnemiesCount)
	{
		mEnemiesLeft[0] = aHUD.mEnemiesLeft[0];
		mEnemiesLeft[1] = aHUD.mEnemiesLeft[1];
	}

	HUD::~HUD()	{

	}

	void HUD::updateHUD(sf::Vector2f coordinates, const int enemiesCount) {
	
		// Setting the HUD
		mHUD.setPosition(sf::Vector2f(coordinates.x - 360,
			coordinates.y - 270));

		mHUD.setString(_itoa(enemiesCount, mEnemiesLeft, 10));

	}

}