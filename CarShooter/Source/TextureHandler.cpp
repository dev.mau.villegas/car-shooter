#include "../Headers/TextureHandler.h"
#include "../Headers/Const.h"

namespace CoreGame {

	TextureHandler::TextureHandler() {

		// Red Car
		textureMap[RED_CAR]		= new sf::Texture();
		textureMap[RED_CAR]->	loadFromFile(	RED_CAR		);

		// Pink Car
		textureMap[PINK_CAR]	= new sf::Texture();
		textureMap[PINK_CAR]->	loadFromFile(	PINK_CAR	);

		// Car Explotion
		textureMap[EXPLOTION]	= new sf::Texture();
		textureMap[EXPLOTION]->	loadFromFile(	EXPLOTION	);

		// White Car
		textureMap[WHITE_CAR]	= new sf::Texture();
		textureMap[WHITE_CAR]->	loadFromFile(	WHITE_CAR	);

		// Yellow Car
		textureMap[YELLOW_CAR]	= new sf::Texture();
		textureMap[YELLOW_CAR]->loadFromFile(	YELLOW_CAR	);

		// Background Level One
		textureMap[BG_LEVEL_ONE] = new sf::Texture();
		textureMap[BG_LEVEL_ONE]->loadFromFile(	BG_LEVEL_ONE);

		for (std::map<std::string, sf::Texture*>::iterator
			it = textureMap.begin(); it != textureMap.end(); ++it) {
			it->second->setSmooth(true);
		}
	}

	// Destructor
	TextureHandler::~TextureHandler() {
		for (std::map<std::string, sf::Texture*>::const_iterator
			 it = textureMap.begin(); it != textureMap.end(); ) {

			delete (it->second);
			it = textureMap.erase(it);
		}
	}

	sf::Texture* TextureHandler::getTexture(const std::string aTextureName) const {
		
		sf::Texture* specificTexture = nullptr;
		std::map<std::string, sf::Texture*>::const_iterator it;

		it = textureMap.find(aTextureName);

		if (it != textureMap.end())
			specificTexture = it->second;

		return specificTexture;
	}

}