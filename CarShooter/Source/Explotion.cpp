#include "../Headers/Explotion.h"

namespace ClassVehicle {

	Explotion::Explotion(sf::Texture* aTexture, sf::Vector2u aImageCount,
		float aSwitchtime, sf::Vector2f aPosition)
		: mAnimation(aTexture, aImageCount, aSwitchtime)
		, mModel(sf::Vector2f(64.0f, 64.0f))
		, mModelTexture(aTexture)
		, mRow(0)
		, mFinished(false)
		, mTotalTime(0.0f)
		, mSwitchTime(aSwitchtime)
	{
		mModel.setPosition(aPosition);
		mModel.setTexture(mModelTexture);
	}

	Explotion::Explotion(const Explotion& aExplotion)
		: mModel		(aExplotion.mModel)
		, mModelTexture	(aExplotion.mModelTexture)
		, mAnimation	(aExplotion.mAnimation)
		, mRow			(aExplotion.mRow)
		, mFinished		(aExplotion.mFinished)
		, mTotalTime	(aExplotion.mTotalTime)
		, mSwitchTime	(aExplotion.mSwitchTime)
	{
	}

	Explotion::~Explotion()	{
	}

	void Explotion::update(float deltaTime)	{
		mTotalTime += deltaTime;
		if (mTotalTime >= mSwitchTime*5) {
			mTotalTime -= mSwitchTime*5;
			if (mRow == 4) {
				mFinished = true;
			} else {
				mRow++;
			}
		}

		if (mFinished == false) {
			mAnimation.update(mRow, deltaTime);
			mModel.setTextureRect(mAnimation.uvRect);
		}
	}

	void Explotion::draw(sf::RenderWindow& aWindow) {
		aWindow.draw(mModel);
	}

}