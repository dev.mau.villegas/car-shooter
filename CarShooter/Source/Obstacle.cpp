#include "../Headers/Obstacle.h"

namespace Collider {

	Obstacle::Obstacle(sf::Vector2f aSize, sf::Vector2f aPosition) {
		mBody.setSize(aSize);
		mBody.setPosition(aPosition);
		mBody.setOrigin(aSize / 2.0f);
	}

	Obstacle::Obstacle(const Obstacle& aObstacle)
		: mBody(aObstacle.mBody)
	{
	}

	Obstacle::~Obstacle()
	{
	}

}