#include "../Headers/CoreGame.h"

#include <SFML/Graphics.hpp>


namespace CoreGame {

	// This is the constructor of the core game algorithm
	CoreGame::CoreGame() : mWindow(sf::VideoMode(800, 600), "Game Name"
						 , sf::Style::Close | sf::Style::Titlebar)
						 , mCurrentState(S_MENU)
						 , mLevelHUD(nullptr)
						 , mLevelOne(nullptr) 
						 , mEnemiesAlive(0)
						 , mMenu(new MainMenu(mWindow))

	{

		// This sets a roof for the FPS
		mWindow.setFramerateLimit(60);
	}

	// The destructor frees thememory allocated
	CoreGame::~CoreGame() {
		delete mLevelOne;
		delete mLevelHUD;
		delete mMenu;
	}

	// Game Loop Function. 
	// It handles the operations while the window is open.
	void CoreGame::runGame() {
		
		// Delta Time stabilizes the floor of FPS
		float deltaTime = 0.0f;
		sf::Clock clock;
		mWindow.setMouseCursorVisible(false);

		while (mWindow.isOpen()) {

			deltaTime = clock.restart().asSeconds();

			sf::Event aEvent;

			// If an event occur, this while reacts.
			while (mWindow.pollEvent(aEvent)) {

				switch (aEvent.type) {
					// To close the window with the X
					case sf::Event::Closed:
						mWindow.close(); break;

					// To close the program with Escape
					case sf::Event::KeyPressed:
						if (aEvent.key.code == sf::Keyboard::Escape)
							mWindow.close(); break;

					// To handle the main menu if it's in there
					case sf::Event::KeyReleased:
						if (mCurrentState == S_MENU) { 
							handleMenu(aEvent);
						}
						break;

					default: break;
				}
			}

			// It controls the flow of the program depending on the State
			switch (mCurrentState) {

				case S_PLAYING:
					mEnemiesAlive = mLevelOne->getEnemiesNumber();
					mLevelOne->generalMovement(deltaTime);
					mLevelOne->shootBullet(deltaTime);
					mLevelHUD->updateHUD(mLevelOne->getViewCoordinates(),
										 mEnemiesAlive);
					mLevelHUD->draw(mWindow);
					if (mEnemiesAlive == 0) {
						playerWon();
					}
					break;
				case S_MENU:
					mMenu->draw();
					break;

				default:
					break;
			}

			mWindow.display();
			mWindow.clear();
		}

	}

	// This handle the Main Menu interaction
	void CoreGame::handleMenu(const sf::Event aEvent)	{

		switch (aEvent.key.code) {
		case sf::Keyboard::W:
			mMenu->moveUp();
			break;
		case sf::Keyboard::S:
			mMenu->moveDown();
			break;
		case sf::Keyboard::Return:
			switch (mMenu->getSelection()) {
			case 0:

				delete mMenu;
				mMenu = nullptr;
				mCurrentState = S_PLAYING;
				mLevelOne = new LevelOne(mWindow);
				mLevelHUD = new HUD((int)mLevelOne->getEnemiesNumber());

				mWindow.clear();
				break;

			case 1:
				mWindow.close();
				break;
			}

			break;

		}
	}


	void CoreGame::playerWon() {
		mCurrentState = S_MENU;
		delete mLevelOne;
		delete mLevelHUD;
		mLevelOne = nullptr;
		mLevelHUD = nullptr;
		mMenu = new WonScreen(mWindow);
	}

}