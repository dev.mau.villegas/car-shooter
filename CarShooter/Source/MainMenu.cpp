#include "../Headers/MainMenu.h"
#include "../Headers/Const.h"
#include <iostream>
namespace CoreGame {

	MainMenu::MainMenu(sf::RenderWindow& aWindow)
					   : GeneralMenu(aWindow)
		
	{
		mElements = MAIN_MENU_ITEMS;

		mMenu[0].setString("PLAY");
		mMenu[0].setFillColor(sf::Color::Red);
		mMenu[1].setString("EXIT");
	}

	MainMenu::MainMenu(const MainMenu & aMainMenu)
		: GeneralMenu(aMainMenu.mWindow)
	{
		mElements = aMainMenu.mElements;

		mMenu[0] = aMainMenu.mMenu[0];
		mMenu[1] = aMainMenu.mMenu[1];
	}

	MainMenu::~MainMenu() {
	}
	
}