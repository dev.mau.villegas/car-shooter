#include "../Headers/Vehicle.h"
#include "../Headers/Const.h"

#include <iostream>

namespace ClassVehicle {

	Vehicle::Vehicle(float modelWidth, float modelHeight,
					 sf::Texture* vehicleTexture,
					 float aMaxSpeed, float aSpawnX, float aSpawnY)
					 : mModel(sf::RectangleShape(sf::Vector2f(modelWidth, modelHeight)))
					 , mModelTexture(vehicleTexture)
					 , mLife(100)
					 , mSpeed(0)
					 , mMaxSpeed(aMaxSpeed)
					 , mSpawn(sf::Vector2f(aSpawnX,aSpawnY))
					 , mTotalTime(0)
	{
		mModel.setTexture(mModelTexture);
		mModel.setOrigin(mModel.getSize().x / 2, mModel.getSize().y / 2);
		mModel.setPosition(mSpawn.x, mSpawn.y);
	}

	Vehicle::Vehicle(const Vehicle& aVehicle) 
		: mModel		(aVehicle.mModel)
		, mModelTexture	(aVehicle.mModelTexture)
		, mBulletList	(aVehicle.mBulletList)
		, mLife			(aVehicle.mLife)
		, mSpeed		(aVehicle.mSpeed)
		, mMaxSpeed		(aVehicle.mMaxSpeed)
		, mSpawn		(aVehicle.mSpawn)
		, mAngle		(aVehicle.mAngle)
		, mTotalTime	(aVehicle.mAngle)
	{
		mModel.setTexture(mModelTexture);
		mModel.setOrigin(mModel.getSize().x / 2, mModel.getSize().y / 2);
		mModel.setPosition(mSpawn.x, mSpawn.y);
	}

	Vehicle::~Vehicle()	{
	}

	void Vehicle::shootBullet(float deltaTime) {
		/*
		mTotalTime += deltaTime;
		if (mTotalTime >= BULLET_DELAY) {
			mTotalTime -= BULLET_DELAY;
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
				mBulletList.push_back(
					Weapon::Bullet(mModel.getPosition(), mAngle, mSpeed)
				);
			}
		}
		*/
	}

	void Vehicle::drawBullet(sf::RenderWindow& aWindow) {

		for (std::vector<Weapon::Bullet>::iterator it = mBulletList.begin();
			 it != mBulletList.end(); ) {
			if (it->getLifeSpan() < 50) {
				it->moveBullet();
				it->riseLifespan();
				aWindow.draw(it->getBullet());
				++it;
			} else {
				it = mBulletList.erase(it);
			}

		}

	}

}