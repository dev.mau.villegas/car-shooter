#include "../Headers/WonScreen.h"
#include "../Headers/Const.h"

#include <iostream>
namespace CoreGame {

	WonScreen::WonScreen(sf::RenderWindow& aWindow)
		: GeneralMenu(aWindow)

	{

		mElements = WON_MENU_ITEMS;

		mMenu[0].setString("PLAY AGAIN");
		mMenu[0].setFillColor(sf::Color::Red);
		mMenu[1].setString("EXIT");

	}

	WonScreen::WonScreen(const WonScreen& aWonScreen)
		: GeneralMenu(aWonScreen.mWindow)
	{
		mElements = aWonScreen.mElements;

		mMenu[0] = aWonScreen.mMenu[0];
		mMenu[1] = aWonScreen.mMenu[0];
	}

	WonScreen::~WonScreen() {
	}

}