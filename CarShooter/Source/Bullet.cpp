#include "../Headers/Bullet.h"
#include "../Headers/Const.h"

#include <iostream>

namespace Weapon {

	Bullet::Bullet(sf::Vector2f aSpawn, float aAngle, float aSpeed)
		: mBody(sf::RectangleShape(sf::Vector2f(5.0f, 5.0f)))
		, mSpawn(aSpawn)
		, mAngle(aAngle)
		, mSpeed(BULLET_SPEED + aSpeed)
		, mLifeSpan(0)
	{
		mBody.setFillColor(sf::Color::Red);
		mBody.setPosition(mSpawn.x, mSpawn.y);
		mBody.rotate(aAngle);

	}

	Bullet::Bullet(const Bullet& aBullet)
		: mBody		(aBullet.mBody)
		, mSpawn	(aBullet.mSpawn)
		, mAngle	(aBullet.mAngle)
		, mSpeed	(aBullet.mSpeed)
		, mLifeSpan (aBullet.mLifeSpan)
	{
	}


	Bullet::~Bullet() {
	}

	void Bullet::moveBullet() {

		float forX = 0.2f*-sin(mAngle);
		float forY = 0.2f*cos(mAngle);

		mBody.move(-forX*mSpeed, -forY*mSpeed);
	}

	

}