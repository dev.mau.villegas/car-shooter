#ifndef ENEMY_VEHICLE_H
#define ENEMY_VEHICLE_H

#include "Vehicle.h"

namespace ClassVehicle {

	class EnemyVehicle : public Vehicle {

		enum vehicleState {
			SELF_DRIVE,
			FOLLOWING
		};

		int mActualIndex;
		sf::Vector2f* mCheckpointsVector;
		int mNumberOfCheckpoints;
		vehicleState mCurrentState;

	public:
		EnemyVehicle(float modelWidth, float modelHeight,
			sf::Texture* vehicleTexture,
			float aMaxSpeed, float aSpawnX, float aSpawnY,
			sf::Vector2f* aCheckpointsVector, int aNumberOfCheckpoints);
		~EnemyVehicle();

		void inteligentMovement(sf::Vector2f& mainPlayerPosition);

		void searchNearestCheckpoint();
		
		sf::Vector2f checkState(sf::Vector2f& mainPlayerPosition);
	};

}

#endif