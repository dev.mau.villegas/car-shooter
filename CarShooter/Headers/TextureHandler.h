#ifndef TEXTURE_HANDLER_H
#define TEXTURE_HANDLER_H

#include "SFML\Graphics.hpp"

namespace CoreGame {

	class TextureHandler {

		std::map<std::string, sf::Texture*> textureMap;

	public:

		// Constructor
		TextureHandler();

		// Destructor
		~TextureHandler();

		// Asking for a specific texture.
		// If it doesn't exist return nullptr
		sf::Texture* getTexture(const std::string aTextureName) const;

	};

}

#endif