#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include "SFML\Graphics.hpp"
#include "GeneralMenu.h"

namespace CoreGame {

	class MainMenu : public GeneralMenu {
		
	public:

		// Constructor
		MainMenu(sf::RenderWindow& aWindow);

		// Copy Constructor
		MainMenu(const MainMenu& aMainMenu);

		// Destructor
		~MainMenu();
		
	};

}
#endif