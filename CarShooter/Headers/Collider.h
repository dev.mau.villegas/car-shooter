#ifndef COLLIDER_H
#define COLLIDER_H

#include <SFML\Graphics.hpp>

namespace Collider {

	class Collider {

		sf::RectangleShape& mBody;

	public:
		Collider(sf::RectangleShape& aBody);
		~Collider();

		bool ckeckCollision(Collider& aOther, float push);

		inline void move(const float xValue, const float yValue) { mBody.move(xValue, yValue); }

		inline sf::Vector2f getPosition() const { return mBody.getPosition(); }
		inline sf::Vector2f getHalfSize() const { return mBody.getSize() / 2.0f; }
	};

}

#endif