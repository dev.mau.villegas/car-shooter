#ifndef WON_SCREEN
#define WON_SCREEN

#include "SFML\Graphics.hpp"
#include "GeneralMenu.h"

namespace CoreGame {

	class WonScreen : public GeneralMenu {

	public:

		// Contrsuctor 
		WonScreen(sf::RenderWindow& aWindow);
		
		// Copy Constructor
		WonScreen(const WonScreen& aWonScreen);

		// Destructor
		~WonScreen();

	};

}

#endif
