#ifndef ANIMATION_H
#define ANIMATION_H

#include <SFML/Graphics.hpp>

namespace CoreGame {

	class Animation {

		sf::Vector2u mImageCount;
		sf::Vector2u mCurrentImage;
		float mTotalTime;
		float mSwitchTime;

	public:

		// Constructor
		Animation(sf::Texture* aTexture,
			sf::Vector2u aImageCount,
			float aSwitchtime);

		// Copy constructor
		Animation(const Animation& aAnimation);

		// Destructor
		~Animation();

		void update(unsigned int aRow, float deltaTime);

		sf::IntRect uvRect;
	};

}


#endif
