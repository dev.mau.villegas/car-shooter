#ifndef BULLET_H
#define BULLET_H

#include <SFML/Graphics.hpp>

#include "Collider.h"

namespace Weapon {

	class Bullet {

		sf::RectangleShape mBody;
		sf::Vector2f mSpawn;
		float mAngle;
		float mSpeed;
		unsigned int mLifeSpan;


	public:

		// Constructor
		Bullet(sf::Vector2f aSpawn, float aAngle, float aSpeed);

		// Copy Constructor
		Bullet(const Bullet& aBullet);

		// Destructor
		~Bullet();

		sf::RectangleShape getBullet() const { return mBody; }

		void moveBullet();

		inline unsigned int getLifeSpan() const { return mLifeSpan; }
		inline void riseLifespan() { mLifeSpan++; }

		Collider::Collider getCollider() {
			return (Collider::Collider(mBody));
		}
	};

}

#endif