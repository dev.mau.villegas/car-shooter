#ifndef GENERAL_MENU
#define GENERAL_MENU

#include "SFML/Graphics.hpp"

namespace CoreGame {

	class GeneralMenu {

	protected:
		std::vector<sf::Text> mMenu;
		sf::Font mFont;
		sf::RenderWindow& mWindow;
		unsigned int mElementSelected;
		unsigned int mElements;
		sf::View mView;

	public:

		// Constructor
		GeneralMenu(sf::RenderWindow& aWindow);

		// Copy Constructor
		GeneralMenu(const GeneralMenu& aGeneralMenu);

		// Destructor
		~GeneralMenu();

		void draw();
		void moveUp();
		void moveDown();

		int getSelection() const { return mElementSelected; }

	};

}

#endif