#ifndef HUD_H
#define HUD_H

#include "SFML\Graphics.hpp"

namespace ClassVehicle {
	class EnemyVehicle;
}

namespace CoreGame {

	class HUD {

		sf::Font mHUDFont;
		char mEnemiesLeft[2];
		sf::Text mHUD;
		unsigned int mEnemiesCount;

	public:

		// Constructor
		HUD(unsigned int aEnemiesCount);

		// Copy Constructor
		HUD(const HUD& aHUD);

		// Destructor
		~HUD();

		void updateHUD(const sf::Vector2f coordinates, const int enemiesCount);
		void draw(sf::RenderWindow& window) { window.draw(mHUD); }
	};

}

#endif 